﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class OnDeletePortfolioImages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProtfolioImages_Portfolios_PortfolioId",
                table: "ProtfolioImages");

            migrationBuilder.AddForeignKey(
                name: "FK_ProtfolioImages_Portfolios_PortfolioId",
                table: "ProtfolioImages",
                column: "PortfolioId",
                principalTable: "Portfolios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProtfolioImages_Portfolios_PortfolioId",
                table: "ProtfolioImages");

            migrationBuilder.AddForeignKey(
                name: "FK_ProtfolioImages_Portfolios_PortfolioId",
                table: "ProtfolioImages",
                column: "PortfolioId",
                principalTable: "Portfolios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
