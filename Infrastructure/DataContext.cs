﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using Microsoft.Extensions.Logging;

namespace Infrastructure
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
             : base(options)
        { }


        //public  DbSet<Address> Addresses { get; set; }
        public  DbSet<ApplicationUsers> ApplicationUsers { get; set; }
        //public  DbSet<EntityBase> EntityBases { get; set; }
        public  DbSet<Owner> Owners { get; set; }
        public  DbSet<Portfolio> Portfolios { get; set; }
        public  DbSet<ProtfolioImage> ProtfolioImages { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=localhost;database=shantuappsdb;user=root;password=");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            /*
            modelBuilder.Entity<EntityBase>(entity =>
            {
                entity.HasKey(e => e.Id);
               
            });
            */
            modelBuilder.Entity<ApplicationUsers>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Email).IsRequired();
                entity.HasOne(a => a.Address)
                    .WithMany(p => p.ApplicationUsers);
            });

            modelBuilder.Entity<Owner>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.FullName).IsRequired();
                entity.HasOne(a => a.Address)
                     .WithMany(p =>p.Owners );
            });

            /*
            modelBuilder.Entity<Address>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Street).IsRequired();
                entity.Property(e => e.City).IsRequired();
                entity.Property(e => e.Number).IsRequired();
                entity.Property(e => e.Country).IsRequired();
               
            });
            */

            
            modelBuilder.Entity<Portfolio>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.ProjectName).IsRequired();                
                entity.Property(e => e.Description).IsRequired(); 
                entity.Property(e => e.LogoUrl).IsRequired(); 
                entity.HasMany(b=>b.ProtfolioImages)
                .WithOne().OnDelete(DeleteBehavior.Cascade);
            });
            
            modelBuilder.Entity<ProtfolioImage>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Description).IsRequired();
                // entity.HasOne(a => a.Portfolio)
                //     .WithMany(p => p.ProtfolioImages);
                //     //.HasForeignKey(p => p.PortfolioId);


            });






        }
    }
}
