﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
   public class Address : EntityBase
    {

        public String Street { get; set; }
        public String City { get; set; }
        public int Number { get; set; }
        public String Country { get; set; }

        public ICollection<Owner> Owners { get; set; }

        public ICollection<ApplicationUsers> ApplicationUsers { get; set; }
    }
}
