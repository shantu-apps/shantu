﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Owner: EntityBase
    {
        public String FullName { get; set; }
        public String profil { get; set; }
        public String Avatar { get; set; }
        public Address Address { get; set; }
    }
}
