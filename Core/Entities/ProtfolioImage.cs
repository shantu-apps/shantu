﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
     public class ProtfolioImage : EntityBase
     {
        public String Description { get; set; }
        public String ImageUrl { get; set; }
     }
}
