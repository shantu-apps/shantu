﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Portfolio : EntityBase 
    {
        public String ProjectName { get; set; }
        public String Description { get; set; }
        public String LogoUrl { get; set; }
        public ICollection<ProtfolioImage> ProtfolioImages { get; set; }

        public Portfolio()
        {
            ProtfolioImages = new List<ProtfolioImage>();
        }
    }
    
}
