﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
   public class ApplicationUsers : EntityBase
   {
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string HowRegistered { get; set; }
        public string Platform { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}
