﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork<Owner> _owner;
        private readonly IUnitOfWork<Portfolio> _portfolio;
        private readonly IUnitOfWork<ProtfolioImage> _portfolioImage;

        public HomeController(IUnitOfWork<Owner> owner,
                              IUnitOfWork<Portfolio> portfolio,
                              IUnitOfWork<ProtfolioImage> portfolioImage)
        {
            _owner = owner;
            _portfolio = portfolio;
            _portfolioImage = portfolioImage;
        }
        public IActionResult Index()
        {
            
            var homeViewModel = new HomeViewModel
            {
                Owner = _owner.Entity.GetAll().First(),
                Portfolios = _portfolio.Entity.GetAll().ToList(),
                ProtfolioImages = _portfolioImage.Entity.GetAll().ToList()
            };

            return View(homeViewModel);
            
         
           //return View();
        }

        /*
        private readonly ILogger<HomeController> _logger;
        private readonly IPortfolioService _iPortfolioService;

        public HomeController(ILogger<HomeController> logger, IPortfolioService portfolioService)
        {
            _logger = logger;
            _iPortfolioService = portfolioService;
       
        }

        public async Task<IActionResult> IndexAsync()
        {
           // _iPortfolioService.GetAllAsync();
          

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        */

    }
}
