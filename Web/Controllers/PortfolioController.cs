﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Controllers
{
    public class PortfolioController : Controller
    {
        private readonly DataContext _context;
        private readonly IUnitOfWork<Portfolio> _portfolio;
        private readonly IUnitOfWork<ProtfolioImage> _portfolioImage;
        private readonly IHostingEnvironment _hosting;

        public PortfolioController(DataContext context,  IUnitOfWork<Portfolio> portfolio, IUnitOfWork<ProtfolioImage> portfolioImage, IHostingEnvironment hosting)
        {
            _context = context;
            _portfolio = portfolio;
            _portfolioImage = portfolioImage;
            _hosting = hosting;
        }

        public IActionResult Index()
        {

            var portfolioViewModel = new PortfolioViewModel
            {            
                Portfolios = _portfolio.Entity.GetAll().ToList(),
               //ProtfolioImages = _portfolioImage.Entity.GetAll().ToList()
            };

            return View(portfolioViewModel);


            
        }

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portfolioViewModel = new PortfolioViewModel
            {

                Portfolio = _context.Portfolios.Include(x=>x.ProtfolioImages)
                            .FirstOrDefault(x => x.Id == id)
            };

            return View(portfolioViewModel);
            
         
           
        }

        public IActionResult Create()
        {
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PortfolioViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.LogoImage != null)
                {
                    string uploads = Path.Combine(_hosting.WebRootPath, @"img\portfolio");
                    string logoPath = Path.Combine(uploads, model.LogoImage.FileName);
                    string imagePath = Path.Combine(uploads, model.ImageFile.FileName);
                    model.LogoImage.CopyTo(new FileStream(logoPath, FileMode.Create));
                    model.ImageFile.CopyTo(new FileStream(imagePath, FileMode.Create));
                }

                Portfolio portfolioItem = new Portfolio
                {
                    ProjectName = model.ProjectName,
                    Description = model.Description,
                    LogoUrl = model.LogoImage.FileName,
                };

                ProtfolioImage protfolioImage = new ProtfolioImage
                {
                    Description = "",
                    ImageUrl = model.ImageFile.FileName,
                };

                portfolioItem.ProtfolioImages.Add(protfolioImage);

                _portfolio.Entity.Insert(portfolioItem);
                //_portfolioImage.Entity.Insert(protfolioImage);
                _portfolio.Save();
                //_portfolioImage.Save();
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        // GET: PortfolioItems/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var portfolioItem = _portfolio.Entity.GetById(id);
            //var ProtfolioImages = _portfolioImage.Entity.GetAll().Where(x => x.Portfolio.Id == id).ToList();
            //var PortfolioImages = _portfolioImage.Entity.GetAll().ToList();

            
             var portfolioItem  = _context.Portfolios.Include(x=>x.ProtfolioImages)
                            .FirstOrDefault(x => x.Id == id);
            var PortfolioImages = portfolioItem.ProtfolioImages.ToList();
           


            if (portfolioItem == null)
            {
                return NotFound();
            }

             var portfolioViewModel = new PortfolioViewModel
            {

                Id = portfolioItem.Id,
                ProjectName = portfolioItem.ProjectName,
                Description = portfolioItem.Description,
                LogoUrl = portfolioItem.LogoUrl,
                protfolioImages = PortfolioImages
                
            };

           

            return View(portfolioViewModel);
        }

        // POST: PortfolioItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, PortfolioViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.LogoImage != null || model.ImageFile != null)
                    {
                        string uploads = Path.Combine(_hosting.WebRootPath, @"img\portfolio");
                        string logoPath = Path.Combine(uploads, model.LogoImage.FileName);
                        string imagePath = Path.Combine(uploads, model.ImageFile.FileName);
                        model.LogoImage.CopyTo(new FileStream(logoPath, FileMode.Create));
                        model.ImageFile.CopyTo(new FileStream(imagePath, FileMode.Create));
                    }


                    //var portfolioItem = _context.Portfolios.Include(x => x.ProtfolioImages)
                    //        .FirstOrDefault(x => x.Id == id);
                    //var PortfolioImages = portfolioItem.ProtfolioImages.ToList();
                    //var portfolioImagesIds = PortfolioImages.Select(x => x.Id).ToList();
                    //if (portfolioImagesIds.Count != 0)
                    //{
                    //    foreach (var item in portfolioImagesIds)
                    //    {
                    //        _portfolioImage.Entity.Delete(item);
                    //        _portfolio.Save();
                    //    }

                    //}

                    ProtfolioImage protfolioImage = new ProtfolioImage
                    {

                        Description = "",
                        ImageUrl = model.ImageFile.FileName,
                    };


                    Portfolio portfolioItem = new Portfolio
                    {
                        Id = model.Id,
                        ProjectName = model.ProjectName,
                        Description = model.Description,
                        LogoUrl = model.LogoImage.FileName,
                                               
                        
                    };


                    portfolioItem.ProtfolioImages.Add(protfolioImage);
                    

                    _portfolio.Entity.Update(portfolioItem);
                    //_portfolioImage.Entity.Update(protfolioImage);
                    
                    //_portfolioImage.Save();
                    _portfolio.Save();


                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortfolioItemExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

       
        // GET: PortfolioItems/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var portfolioViewModel = new PortfolioViewModel
            {
                Portfolio = _context.Portfolios.Include(x => x.ProtfolioImages)
                            .FirstOrDefault(x => x.Id == id)
                //Portfolio = _portfolio.Entity.GetAll().FirstOrDefault(x => x.Id == id),
            };

            return View(portfolioViewModel);
           
        }

        // POST: PortfolioItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            
                  
            _portfolio.Entity.Delete(id);
            _portfolio.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool PortfolioItemExists(int id)
        {
            return _portfolio.Entity.GetAll().Any(e => e.Id == id);
        }
    }


}

