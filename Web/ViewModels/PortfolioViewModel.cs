﻿using Core.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class PortfolioViewModel
    {
        public int Id { get; set; }

        public String ProjectName { get; set; }
        public String Description { get; set; }
        public String LogoUrl { get; set; }
        public IFormFile LogoImage { get; set; }

        public List<Portfolio> Portfolios { get; set; }

        public String ImageDescription { get; set; }
        public String ImageUrl { get; set; }
        

        public List<ProtfolioImage> protfolioImages { get; set; }

        public Portfolio Portfolio { get; set; }

        public IFormFile ImageFile { get; set; }


    }
}
