-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 19. Feb 2021 um 18:50
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `shantuappsdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `address`
--

CREATE TABLE `address` (
  `Id` int(11) NOT NULL,
  `Street` longtext DEFAULT NULL,
  `City` longtext DEFAULT NULL,
  `Number` int(11) NOT NULL,
  `Country` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `applicationusers`
--

CREATE TABLE `applicationusers` (
  `Id` int(11) NOT NULL,
  `Firstname` longtext DEFAULT NULL,
  `LastName` longtext DEFAULT NULL,
  `Email` longtext NOT NULL,
  `Password` longtext DEFAULT NULL,
  `AddressId` int(11) DEFAULT NULL,
  `Phone` longtext DEFAULT NULL,
  `Gender` longtext DEFAULT NULL,
  `HowRegistered` longtext DEFAULT NULL,
  `Platform` longtext DEFAULT NULL,
  `RegistrationDate` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `owners`
--

CREATE TABLE `owners` (
  `Id` int(11) NOT NULL,
  `FullName` longtext NOT NULL,
  `profil` longtext DEFAULT NULL,
  `Avatar` longtext DEFAULT NULL,
  `AddressId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `owners`
--

INSERT INTO `owners` (`Id`, `FullName`, `profil`, `Avatar`, `AddressId`) VALUES
(1, 'Asif Hossain Shantu', 'Wellcome to official website of \'Asif Hossain Shantu\'.', 'shantuN.jpg', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio`
--

CREATE TABLE `portfolio` (
  `Id` int(11) NOT NULL,
  `ProjectName` longtext DEFAULT NULL,
  `Description` longtext DEFAULT NULL,
  `LogoUrl` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `portfolio`
--

INSERT INTO `portfolio` (`Id`, `ProjectName`, `Description`, `LogoUrl`) VALUES
(1, '29 CARD ONLINE', '\'29 Card online\' is an Online Multiplayer game. Facebook users or guest players can create room to play with other online players. Maximum 4 players can play and several viwers can view as a visitor in an ongoing online game. Robots are also there as Partner and/or oppenent in an online game.\r\nFor standard rules please check this wiki page.\r\n\r\n', '29 logo.png'),
(3, 'Accident Management System', 'Manage your Accident cases, Driver Information, Passenger Information, London Solicitors, Accident Images and documents as attachment, CNF generation, invoice generation, case History management, invoice generation, reminder system, Payment system etc\r\n\r\nused by G5 Group, London since 2010 as Intranet WIFI application in G5 Group office.\r\n\r\nFeatures:\r\n\r\nSystem Runs on intranet / wifi or internet. view demo system here Login as Demo User\r\nASP .Net Application with MySql Database (NHibernate)\r\nDifferent User Roles: Admin, Manager\r\nManage Cases any time, add History in ongoing cased, payment information, add reminder to a case and many more\r\nManage Driver Information, or Passengers, Witnesses and Third Party information\r\nSolicitor Management\r\nCar accident Images as Attachments, upload any document to a case as attachment.\r\nComplete History, Reminder, Payment, Invoices, User Profile, Crash care center etc\r\nand many more features..Login as Demo User\r\nFor purchase with source code or installation in any hosting environement please contact.', 'AccidentLogo.png'),
(5, 'my Money', 'Available in ShantuApps.de and Facebook Login with Facebook\r\nManage your payments, either paid or received, for yourself or for your friends, personal or business, keep track of your payments like a personal online bank, share and connect with your friends, save pdf file to your pc or email it and many more, checkout my Money\r\n\r\nFeatures:\r\n\r\nStandalone system my Money Login as Demo User\r\nConnected with FaceBook for social actions my Money app in Facebook\r\nSave Money Received or Money Paid\r\nSearch by title or amount, description and many more. View searched total\r\nShare with facebook friends, post and many more..\r\nDownload your Data as PDF file\r\nDelete all money info by one signle click\r\nShow/Hide your data in App page\r\nDelete Account any time..\r\nand many more features..\r\nFor feature requests, Latest news, voting, events and other interesting topics, please visit', 'myMoneyLogo.png'),
(6, 'my Encryption', 'Available in ShantuApps.de\r\nEncrypt any of your condifential data (text format) before you share or store any where. Instead of plain human readable text, generate encrypted text using your own secret word. Only you can decrypt it using your secret word.\r\n\r\nFeatures:\r\n\r\nSimple standalone system my Encryption\r\nEncrypt using any secret word\r\nDecrypt your text data any time in future for free\r\nWe do not store any of your confidential texts as well as encrypted texts into our system. So please do not forget your secret word.', 'myEncryptionx.png');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `protfolioimages`
--

CREATE TABLE `protfolioimages` (
  `Id` int(11) NOT NULL,
  `Description` longtext NOT NULL,
  `ImageUrl` longtext DEFAULT NULL,
  `PortfolioId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `protfolioimages`
--

INSERT INTO `protfolioimages` (`Id`, `Description`, `ImageUrl`, `PortfolioId`) VALUES
(1, '29 logo', '29 logo.png', 1),
(2, 'Dashboard', 'Dashboard.jpg', 1),
(3, 'Accident Management System', 'AccidentManeg.png', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `__efmigrationshistory`
--

CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `__efmigrationshistory`
--

INSERT INTO `__efmigrationshistory` (`MigrationId`, `ProductVersion`) VALUES
('20201207210655_InitialCreate', '3.1.10'),
('20210130222936_initialmigration', '3.1.10');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`Id`);

--
-- Indizes für die Tabelle `applicationusers`
--
ALTER TABLE `applicationusers`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ApplicationUsers_AddressId` (`AddressId`);

--
-- Indizes für die Tabelle `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Owners_AddressId` (`AddressId`);

--
-- Indizes für die Tabelle `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`Id`);

--
-- Indizes für die Tabelle `protfolioimages`
--
ALTER TABLE `protfolioimages`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ProtfolioImages_PortfolioId` (`PortfolioId`);

--
-- Indizes für die Tabelle `__efmigrationshistory`
--
ALTER TABLE `__efmigrationshistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `address`
--
ALTER TABLE `address`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `applicationusers`
--
ALTER TABLE `applicationusers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `owners`
--
ALTER TABLE `owners`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `protfolioimages`
--
ALTER TABLE `protfolioimages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `applicationusers`
--
ALTER TABLE `applicationusers`
  ADD CONSTRAINT `FK_ApplicationUsers_Address_AddressId` FOREIGN KEY (`AddressId`) REFERENCES `address` (`Id`);

--
-- Constraints der Tabelle `owners`
--
ALTER TABLE `owners`
  ADD CONSTRAINT `FK_Owners_Address_AddressId` FOREIGN KEY (`AddressId`) REFERENCES `address` (`Id`);

--
-- Constraints der Tabelle `protfolioimages`
--
ALTER TABLE `protfolioimages`
  ADD CONSTRAINT `FK_ProtfolioImages_Portfolio_PortfolioId` FOREIGN KEY (`PortfolioId`) REFERENCES `portfolio` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
